package com.example.TienBox;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.TienBox.persistencia.DbProductos;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class VerArticulos extends Fragment {

    List<ListArticulos> listArticulos;
    RecyclerView recycler;

    public VerArticulos() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_ver_articulo, container, false);

        FloatingActionButton btnCrear = root.findViewById(R.id.btnCrear);
        btnCrear.setOnClickListener(view -> ((CardsItems) getActivity()).switchWindow(new CrearArticulo()));

        DbProductos db = new DbProductos(root.getContext());
        listArticulos = db.consultarArticulos();

        recycler = root.findViewById(R.id.recyclerId);
        recycler.setLayoutManager(new LinearLayoutManager(this.getContext(), LinearLayoutManager.VERTICAL, false));
        AdapterArticulos adaptador = new AdapterArticulos(listArticulos, getActivity());
        recycler.setAdapter(adaptador);

        FloatingActionButton btnSalir = root.findViewById(R.id.btnSalir);
        //btnSalir.setOnClickListener(view -> {
        //    ((CardsItems) getActivity()).finish();
        //    System.exit(0);
        btnSalir.setOnClickListener(view -> {
            ((CardsItems) getActivity()).switchWindow(new MapsFragment());
        });

        return root;
    }
}
