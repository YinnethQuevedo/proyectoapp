package com.example.TienBox.persistencia;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import androidx.annotation.Nullable;

public class DbUsuarios extends DbHelper{

    Context context; // Variable global

    // Constructor
    public DbUsuarios(@Nullable Context context) {

        super(context);
        this.context = context;
    }

    // Long: Es de tipo entero de mayor tamaño


    // NEW
    public Boolean insertarUsuario(String nomusuario, String contrasena, String correo){
        DbHelper dbHelper = new DbHelper(context); // Intancia del objeto DbHleper = nuestra base de datos
        SQLiteDatabase db = dbHelper.getWritableDatabase(); // Agregamos los datos

        ContentValues values = new ContentValues(); // Instancia del objeto values
        values.put("nomusuario", nomusuario);
        values.put("contrasena", contrasena);
        values.put("correo", correo);

        long result = db.insert(TABLE_USERS, null, values);
        if (result == -1) return false;
        else
            return true;

    }

    public Boolean checknomusuario(String nomusuario){
        DbHelper dbHelper = new DbHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase(); //
        Cursor cursor = db.rawQuery("SELECT * FROM usuarios WHERE nomusuario =?", new String[] {nomusuario});
        if(cursor.getCount()>0)
            return true;
        else
            return false;
    }

    public Boolean checkcontrasena(String nomusuario, String contrasena){
        DbHelper dbHelper = new DbHelper(context); // Instaciamos nuestra conexión
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM usuarios WHERE nomusuario =? and contrasena=?", new String[] {nomusuario, contrasena});
        if(cursor.getCount()>0)
            return true;
        else
            return false;
    }
}




