package com.example.TienBox.persistencia;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION=3;
    private static final String DATABASE_NOMBRE = "proyecttienbox.db";
    public static final String TABLE_USERS = "usuarios";
    public static final String TABLE_PRODUCTOS = "productos";

    //Constructor
    public DbHelper(@Nullable Context context) {
        super(context, DATABASE_NOMBRE, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(" CREATE TABLE IF NOT EXISTS " + TABLE_USERS + "("+
                "idusuario INTEGER PRIMARY KEY AUTOINCREMENT," +
                "nomusuario TEXT NOT NULL,"+
                "contrasena TEXT NOT NULL,"+
                "correo TEXT NOT NULL)");

        sqLiteDatabase.execSQL(" CREATE TABLE IF NOT EXISTS " + TABLE_PRODUCTOS + "("+
                "codigop INTEGER PRIMARY KEY AUTOINCREMENT," +
                "nombrep TEXT NOT NULL,"+
                "precio DECIMAL NOT NULL,"+
                "localizacion TEXT NOT NULL,"+
                "clasificacion TEXT NOT NULL,"+
                "descripcion TEXT NOT NULL)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        sqLiteDatabase.execSQL(" DROP TABLE IF EXISTS " + TABLE_USERS);// Consulta
        onCreate(sqLiteDatabase);

    }
}
