package com.example.TienBox.persistencia;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import androidx.annotation.Nullable;

import com.example.TienBox.ListArticulos;

import java.util.ArrayList;
import java.util.List;

public class DbProductos extends DbHelper{

    Context context; // Variable global

    // Constructor
    public DbProductos(@Nullable Context context){
        super(context);
        this.context = context;

    }

    public void agregarArticulo(String nombrep, String descripcion, String precio, String clasificacion,String localizacion) {
        ContentValues cv = new ContentValues(); // Instancia del objeto ContentValues
        cv.put("nombrep", nombrep);
        cv.put("descripcion", descripcion);
        cv.put("precio", precio);
        cv.put("clasificacion", clasificacion);
        cv.put("localizacion", localizacion);
        this.getWritableDatabase().insert("productos", null, cv);
    }

    public void eliminarArticulo(String codigop) {
        this.getWritableDatabase().delete("productos", "codigop = ?", new String[]{codigop.trim()});
    }

    public void actualizarArticulo(String codigop, String descripcion, String precio, String localizacion, String clasificacion) {
        ContentValues cv = new ContentValues(); // Instancia
        cv.put("descripcion", descripcion);
        cv.put("precio", precio);
        cv.put("clasificacion", clasificacion);
        cv.put("localizacion", localizacion);
        this.getWritableDatabase().update("productos", cv, "codigop = ?", new String[]{codigop.trim()});
    }

    public List<ListArticulos> consultarArticulos() {
        List<ListArticulos> listArticulos = new ArrayList<ListArticulos>();

        Cursor result = this.getWritableDatabase().query("productos", new String[]{"codigop", "nombrep", "descripcion", "precio", "localizacion", "clasificacion"}, null, null, null, null, null);
        while (result.moveToNext()) {
            ListArticulos nuevoArticulo = new ListArticulos(
                    result.getInt((int) result.getColumnIndex("codigop")),
                    result.getString((int) result.getColumnIndex("nombrep")),
                    result.getString((int) result.getColumnIndex("descripcion")),
                    result.getFloat((int) result.getColumnIndex("precio")),
                    result.getString((int) result.getColumnIndex("localizacion")),
                    result.getString((int) result.getColumnIndex("clasificacion"))
            );
            listArticulos.add(nuevoArticulo);
        }

        return listArticulos;
    }


}
