package com.example.TienBox;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.example.TienBox.persistencia.DbUsuarios;
import com.google.android.material.button.MaterialButton;

public class MainActivity extends AppCompatActivity {

    EditText username, password;
    Button loginbtn, btnRegis;
    DbUsuarios DB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().hide(); // Eliminamos la barrar superior

        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        MaterialButton loginbtn = (MaterialButton) findViewById(R.id.loginbtn);
        MaterialButton btnRegis = (MaterialButton) findViewById(R.id.btnRegis);

        DB = new DbUsuarios(this);

        loginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String user = username.getText().toString();
                String pass = password.getText().toString();

                if (TextUtils.isEmpty(user) || TextUtils.isEmpty(pass))
                    Toast.makeText(MainActivity.this, "Todos los espacios son requeridos", Toast.LENGTH_SHORT).show();
                else {
                    Boolean checkcontrasena = DB.checkcontrasena(user, pass);
                    if (checkcontrasena == true) {
                        // Toast.makeText(MainActivity.this, "Login correcto", Toast.LENGTH_SHORT).show();
                        Dialogo d = new Dialogo(MainActivity.this,"Bienvenido","¿Esta seguro de que desea continuar?", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(getApplicationContext(), CardsItems.class);
                                startActivity(intent);
                            }
                        });
                    } else {
                        Toast.makeText(MainActivity.this, "Login incorrecto", Toast.LENGTH_SHORT).show();
                    }
                }
            }

        });

        btnRegis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Cambiamos de layout/Vista
                Intent intent = new Intent(getApplicationContext(), Registro.class);
                startActivity(intent);
            }
        });
    }
}
