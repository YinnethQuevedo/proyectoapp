package com.example.TienBox;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.TienBox.persistencia.DbUsuarios;
public class Registro extends AppCompatActivity {
    EditText username1, password1, repassword, correo; //nombreid;

    Button btnRegistro, volverSining; // Variable Global
    DbUsuarios DB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        username1 = findViewById(R.id.username1);
        correo = findViewById(R.id.correo);
        password1 = findViewById(R.id.password1);
        repassword = findViewById(R.id.repassword);

        //nombreid = findViewById(R.id.nombreid);


        btnRegistro = findViewById(R.id.btnRegistro);
        volverSining = findViewById(R.id.volverSining);
        DB = new DbUsuarios(this);

        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String user = username1.getText().toString(); //
                String email = correo.getText().toString();
                String pass = password1.getText().toString();
                String repass = repassword.getText().toString();
                //String nameid = nombreid.getText().toString();

                if (TextUtils.isEmpty(user) || TextUtils.isEmpty(email) || TextUtils.isEmpty(pass) || TextUtils.isEmpty(repass))
                    Toast.makeText(Registro.this, "Se requiere llenar los campos", Toast.LENGTH_SHORT).show();
                else{
                    if (pass.equals(repass)) {
                        Boolean checkuser = DB.checknomusuario(user);
                        if (checkuser == false) {
                            Boolean insert = DB.insertarUsuario(user, pass, email);
                            if (insert == true) {
                                Toast.makeText(Registro.this, "Registrado correctamente", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intent);

                            } else {
                                Toast.makeText(Registro.this, "Registro fallido", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(Registro.this, "El usuario ya existe", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(Registro.this, "La contraseña no coincide", Toast.LENGTH_SHORT).show();
                    }
                }
            }

        });

        volverSining.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new  Intent(getApplicationContext(),MainActivity.class);
                startActivity(intent);

            }
        });

    }
}


            //MaterialButton loginbtn = (MaterialButton) findViewById(R.id.loginbtn);

        // setOnClickListener = Cuando damos click al boton de registrarse nos conecta a la base de datos
        // es decir que esta función ejecuta una acción al dar click a un botón.
//        btnRegistro.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                DbHelper dbHelper = new DbHelper(Registro.this); // Instancia de un objeto Conexion a la bd
//                SQLiteDatabase db = dbHelper.getWritableDatabase(); //Crearla = Escribir
//                // Validación
//                if(db != null){
//                    Toast.makeText(Registro.this, "BASE DE DATOS CREADA",Toast.LENGTH_LONG).show();
//                }else {
//                    Toast.makeText(Registro.this,"ERROR AL CREAR BASE DE DATOS", Toast.LENGTH_LONG).show();
//                }
//
//            }
//        });
